Instalación de la librería 3d tiltJs

<!-- Instalar por medio del comando npm: -->
npm install --save tilt.js


<!-- aplicación en Angular -->
<!-- En el componente.ts importamos el repositorio de tilt en nuesto componente: -->
import Tilt from 'tilt.js';

ngOnInit(){

  <!-- inicializamos la librería dentro de nuestro componente -->
  Tilt;

  <!-- y definimos en que elemento del DOM se aplicará la librería -->
  const tilts = $('.contentImgs').tilt({
    maxTilt: 40,
    glare: true,
    maxGlare: 0.3,
    scale: 1.2,
    transition:true,
    speed: 2000,
    easing: "cubic-bezier(.03,.98,.52,.99)",
    perspective:1000,
    reset:false
  });
}



<!-- aplicación sin framework -->

<body>
    <div data-tilt></div> <!-- Tilt element -->
    <script src="jquery.js" ></script> <!-- Load jQuery first -->
    <script src="tilt.js"></script> <!-- Load Tilt.js library -->
</body>

<script>
    const tilt = $('.js-tilt').tilt();
    tilt.on('change', callback);  
    <!-- // parameters: event, transforms -->
    tilt.on('tilt.mouseLeave', callback);
    <!-- // parameters: event -->
    tilt.on('tilt.mouseEnter', callback);
    <!-- // parameters: event -->
</script>
