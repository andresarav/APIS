// guía para firebase

// iniciamos instalando los repositorios con npm
npm install firebase @angular/fire --save

// añadimos en "./src/environments/environment.prod" y en "./src/environments/environment"

export const environment = {
  production: false,
  firebase: {
    apiKey: '<your-key>',
    authDomain: '<your-project-authdomain>',
    databaseURL: '<your-database-URL>',
    projectId: '<your-project-id>',
    storageBucket: '<your-storage-bucket>',
    messagingSenderId: '<your-messaging-sender-id>'
  }
};
