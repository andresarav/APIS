<!-- instalación de countdown.js con npm -->
    npm i -s countdown

<!-- importar la libería a tu componente.ts -->
    import Countdown from 'countdown'

<!-- dentro del ngOnInit inicializamos nuestro componente pasandole los siguientes parametros -->
  var fecha;
  var timerId = new Countdown( new Date(2018, 12, 3), (ts)=> {fecha = ts}, Countdown.HOURS|Countdown.MINUTES|Countdown.SECONDS)

  <!-- para visualizar la fecha en un lenguaje mas humano debemos parsearla a  string -->
  fecha.toString()


<!-- pasamos la fecha, creamos un callback que nos retorna la actualización de la fecha cada segundo, osea nos genera el countdown, los ultimos parametros son los formatos que nos retornaran, tambien se pueden utilizar estos: -->

countdown.ALL =
	countdown.MILLENNIA |
	countdown.CENTURIES |
	countdown.DECADES |
	countdown.YEARS |
	countdown.MONTHS |
	countdown.WEEKS |
	countdown.DAYS |
	countdown.HOURS |
	countdown.MINUTES |
	countdown.SECONDS |
	countdown.MILLISECONDS;

countdown.DEFAULTS =
	countdown.YEARS |
	countdown.MONTHS |
	countdown.DAYS |
	countdown.HOURS |
	countdown.MINUTES |
	countdown.SECONDS;

<!-- PARA AMPLIAR LA INFORMACIÓN SOBRE ESTA LIBRERÍA, CONSULTA EN:--> https://www.npmjs.com/package/countdown
