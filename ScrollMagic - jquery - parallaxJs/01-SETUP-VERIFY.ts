
Hola,

// 1.INSTALAR scroll magic e imports-loader

npm install --save scrollmagic
npm install --save imports-loader // this is important

// ------- PUNTO OPCIONAL DE REFERENCIA ------------- >> Importar la definición de TypeScript desde aquí https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/gsap OJO ESTO SOLO ES REFERENCIA, ESTE PUNTO PUEDE SER IGNORADO, EL PUNTO SIGUIENTE ES EL IMPORTANTE

// 2.instalación correcta de gsap para compatibilidad con angular 6 en: https://medium.com/@mr.frag85/using-gsap-with-angular-6-project-it-works-on-prod-too-9ac036f21487
npm install --save gsap
npm install --save-dev @types/greensock

// en el archivo angular.json agregar LO SIGUIENTE >>>

// ########### ./angular.json #########################################################################################################################
// ##################################################################################################################################################
// "build": {
//   "builder": "@angular-devkit/build-angular:browser",
//   "options": {
//     "outputPath": "dist/xeuxExchange",
//     "index": "src/index.html",
//     "main": "src/main.ts",
//     "polyfills": "src/polyfills.ts",
//     "tsConfig": "src/tsconfig.app.json",
//     "assets": [
//       "src/favicon.ico",
//       "src/assets",
//       "src/manifest.json"
//     ],
//     "styles": [
//       "src/styles.css"
//     ],
    "scripts": [
      "./node_modules/gsap/src/uncompressed/TweenMax.js",
      "./node_modules/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js",
      "./node_modules/scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js",
      "./node_modules/scrollmagic/scrollmagic/uncompressed/plugins/debug.addindicators.js"
    // ]
  // },

// ##################################################################################################################################################
// ##################################################################################################################################################



import { Component, OnInit  } from '@angular/core';
//tambien tenemos instalado jquery ya que es necesario para que esta librería funcione
declare var jQuery:any;
declare var $:any;
//declaramos e importamos las librerías de scrollmagic y gsap TweenMax
declare let ScrollMagic: any;
import {Power1, Bounce} from 'gsap/all';
declare var TweenMax: any;


@Component({
  selector: 'home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})


export class HomeComponent implements OnInit {
  public controller :any;


 ngOnInit(){
      console.log('funciona', ScrollMagic);

       this.controller = new ScrollMagic.Controller({addIndicators: true});

       var scene = new ScrollMagic.Scene({
       offset: 40,
       triggerHook:0.1,
        duration: 120
      })
               .setTween(TweenMax.to("#btcPriceCont", 2, {opacity: 0, ease: Power1.easeOut}))
               .addIndicators()
               .addTo(this.controller);

}

}



// Guía instalación jquery
npm install jquery — save

// añadir el script en el angular.json
"scripts": [ "../node_modules/jquery/dist/jquery.min.js" ]

// en el componente:
import * from 'jquery';
// (or)
declare var $: any;
// (or)
import * as $ from 'jquery';



// Guía instalación PARALLAX.JS
 npm i -s parallax-js

 // en scripts de angular.json
 node_modules/parallax-js/dist/parallax.min.js

 // importar en el componente.ts que se aplicará de la siguiente forma:
 import Parallax from 'parallax-js';

     var scene = document.getElementById('scene');
     var parallaxInstance = new Parallax(scene);

// en el componente.html :

<div id="scene">
  <div data-depth="0.2">My first Layer!</div>
  <div data-depth="0.6">My second Layer!</div>
</div>
